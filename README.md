# PingCAP Talent Plan

[PingCAP_W]: https://pingcap.com
[PingCAP_T_P]: https://github.com/pingcap/talent-plan

This repo will track my progress through the [PingCAP Talent Plan][PingCAP_T_P].

## Licenses

The [PingCAP Talent Plan][PingCAP_T_P] courses may be freely used and modified for any purpose, under the terms of each
course's individual license. [See the courses for details][PingCAP_T_P].

The PingCAP logo, and all associated logos and designs are trademarks or registered trademarks of [PingCAP][PingCAP_W] Inc.
All other trademarks are the property of their respective owners.

---

My progress through completing these tasks is distributed under the terms of both the MIT license and the Apache License
(Version 2.0).

[LICENSE-A]: https://gitlab.com/06kellyjac/pingcap_talent_plan/blob/master/LICENSE-APACHE
[LICENSE-M]: https://gitlab.com/06kellyjac/pingcap_talent_plan/blob/master/LICENSE-MIT

See [LICENSE-APACHE][LICENSE-A] and [LICENSE-MIT][LICENSE-M] for details.
