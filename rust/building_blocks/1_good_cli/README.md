# Exercise: Write a Good CLI Program

[good_cli_program_exercise]: https://qiita.com/tigercosmos/items/678f39b1209e60843cc3
[tigercosmos_qiita]: https://qiita.com/tigercosmos

Complete the exercise [Write a Good CLI Program][good_cli_program_exercise] by [tigercosmos][tigercosmos_qiita] on Qiita.

## Example Rust CLI tools

[ogham/exa](https://github.com/ogham/exa)

[peltoche/lsd](https://github.com/Peltoche/lsd)
