use clap::{App, Arg};
use failure::Error;

fn main() -> Result<(), Error> {
	const NAME: &'static str = env!("CARGO_PKG_NAME");
	const DESCRIPTION: &'static str = env!("CARGO_PKG_DESCRIPTION");
	const AUTHORS: &'static str = env!("CARGO_PKG_AUTHORS");
	const VERSION: &'static str = env!("CARGO_PKG_VERSION");

	let matches = App::new(NAME)
		.version(VERSION)
		.author(AUTHORS)
		.about(DESCRIPTION)
		.arg(
			Arg::with_name("config")
				.short("c")
				.long("config")
				.value_name("FILE")
				.help("Sets a custom config file")
				.takes_value(true),
		)
		.arg(
			Arg::with_name("NAME")
				.help("Person to say hello to")
				.required(false)
				.index(1),
		)
		.arg(
			Arg::with_name("v")
				.short("v")
				.multiple(true)
				.help("Sets the level of verbosity"),
		)
		.get_matches();

	let name = matches.value_of("NAME").unwrap_or("world");

	println!("Hello, {}!", name);

	match matches.occurrences_of("v") {
		0 => println!("No verbose info"),
		1 => println!("Some verbose info"),
		2 => println!("Tons of verbose info"),
		3 | _ => println!("Don't be crazy"),
	}

	Ok(())
}
