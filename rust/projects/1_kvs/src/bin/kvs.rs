use clap::{App, Arg, SubCommand};
use std::process::exit;

fn main() {
	const NAME: &'static str = env!("CARGO_PKG_NAME");
	const DESCRIPTION: &'static str = env!("CARGO_PKG_DESCRIPTION");
	const AUTHORS: &'static str = env!("CARGO_PKG_AUTHORS");
	const VERSION: &'static str = env!("CARGO_PKG_VERSION");

	let matches = App::new(NAME)
		.version(VERSION)
		.author(AUTHORS)
		.about(DESCRIPTION)
		.subcommand(
			SubCommand::with_name("set")
				.about("Set a key")
				.arg(
					Arg::with_name("key")
						.value_name("KEY")
						.help("A key")
						.required(true)
						.takes_value(true),
				)
				.arg(
					Arg::with_name("value")
						.value_name("VALUE")
						.help("A value for the key")
						.required(true)
						.takes_value(true),
				),
		)
		.subcommand(
			SubCommand::with_name("get").about("Get a key").arg(
				Arg::with_name("key")
					.value_name("KEY")
					.help("A key")
					.required(true)
					.takes_value(true),
			),
		)
		.subcommand(
			SubCommand::with_name("rm").about("Remove a key").arg(
				Arg::with_name("key")
					.value_name("KEY")
					.help("A key")
					.required(true)
					.takes_value(true),
			),
		)
		.get_matches();
	match matches.subcommand() {
		("set", Some(_matches)) => {
			eprintln!("unimplemented");
			exit(1);
		}
		("get", Some(_matches)) => {
			eprintln!("unimplemented");
			exit(1);
		}
		("rm", Some(_matches)) => {
			eprintln!("unimplemented");
			exit(1);
		}
		_ => unreachable!(),
	}
}
