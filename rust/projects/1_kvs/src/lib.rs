#![deny(missing_docs)]
//! A key value store.

/// The struct for the key value store
pub struct KvStore {}

/// The methods
impl KvStore {
	/// Create a new KvStore
	pub fn new() -> Self {
		Self {}
	}

	/// Get a value by the key
	pub fn get(&self, key: String) -> Option<String> {
		panic!();
	}

	/// Remove a value by the key
	pub fn remove(&self, key: String) {
		panic!();
	}

	/// Set a key and value
	pub fn set(&self, key: String, value: String) {
		panic!();
	}
}
